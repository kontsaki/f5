use std::os::unix::fs::PermissionsExt;
use std::{ffi::OsString, io::Write};

use clap::Parser;
use miette::{Context, IntoDiagnostic, Result};
use thirtyfour::prelude::*;
use tokio::process;
use walkdir::WalkDir;
use watchexec::{
    action::Action,
    config::{InitConfig, RuntimeConfig},
    error,
    handler::PrintDebug,
    Watchexec,
};

#[cfg(target_os = "linux")]
static GECKODRIVER: &'static [u8] = include_bytes!("../drivers/geckodriver");

#[derive(Parser)]
#[command(version)]
struct Cli {
    url: String,
}

#[tokio::main]
async fn main() -> Result<()> {
    let cli = Cli::parse();

    let mut geckodriver = tempfile::Builder::new()
        .prefix("geckodriver")
        .rand_bytes(0)
        .tempfile()
        .into_diagnostic()?;
    geckodriver.write(GECKODRIVER).into_diagnostic()?;

    let (file, path) = geckodriver.keep().into_diagnostic()?;
    let mut perms = file.metadata().into_diagnostic()?.permissions();
    perms.set_mode(0o700);
    file.set_permissions(perms).into_diagnostic()?;
    drop(file);

    let mut backend = process::Command::new(path).spawn().unwrap();

    let caps = DesiredCapabilities::firefox();
    let driver = WebDriver::new("http://localhost:4444", caps)
        .await
        .into_diagnostic()?;

    // TODO: open dev tools
    if let Err(err) = driver.goto(cli.url).await.into_diagnostic() {
        eprintln!("Cannot visit URL: {}", err);
    }

    let watcher = watch(driver.clone()).await;

    driver.quit().await.into_diagnostic()?;
    backend.kill().await.into_diagnostic()?;
    watcher?;
    Ok(())
}

async fn watch(driver: WebDriver) -> Result<()> {
    let files = WalkDir::new(".")
        .into_iter()
        .filter_map(|e| e.ok())
        .filter(|e| e.file_type().is_file() && !e.file_name().to_string_lossy().starts_with("."))
        .map(|e| e.into_path());

    let mut init = InitConfig::default();
    init.on_error(PrintDebug(std::io::stderr()));

    let mut runtime = RuntimeConfig::default();
    let files: Vec<_> = files.collect();
    println!("Watching Files: {files:?}");
    runtime.pathset(files);

    runtime.on_action(move |a: Action| {
        println!("Action {a:?}");

        let refresher = driver.clone();
        async move {
            // FIXME: handle signals (ctrl-C)
            if let Err(_) = refresher.refresh().await {
                eprintln!("Refreshing failed.. waiting a little bit for browser to breath.");
                tokio::time::sleep(std::time::Duration::from_secs(1)).await;
            };
            Ok::<(), error::RuntimeError>(())
        }
    });

    let we = Watchexec::new(init, runtime)?;
    we.main().await.into_diagnostic()?.into_diagnostic()?;
    Ok(())
}
